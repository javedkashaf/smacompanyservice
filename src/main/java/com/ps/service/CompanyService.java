package com.ps.service;

import java.util.List;

import com.ps.entity.Company;

public interface CompanyService {
	
	Long createCompany(Company cob);
	void updateCompany(Company cob);
	Company getOneCompany(Long id);
	List<Company> getAllCompanies();
	
	public void deleteComapny(Long id);
	boolean isExist(Long id);

}
