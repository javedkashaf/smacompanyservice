package com.ps.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ps.entity.Company;
import com.ps.exception.CompanyNotFoundException;
import com.ps.repo.CompanyRepository;
import com.ps.service.CompanyService;


@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyRepository repo;
	
	@Override
	public Long createCompany(Company cob) {
		return repo.save(cob).getId();
	}

	@Override
	public void updateCompany(Company cob) {
		if(cob.getCregId()!=null && repo.existsById(cob.getId()))
			repo.save(cob);
	}

	@Override
	public Company getOneCompany(Long id) {
		Optional<Company> opt = repo.findById(id);
		if(opt.isEmpty()) {
		throw new CompanyNotFoundException("Given '"+id+"' is not Exist");
		} else {
			return opt.get();
		}
	}

	@Override
	public List<Company> getAllCompanies() {
		return repo.findAll();
	}

	

	@Override
	public boolean isExist(Long id) {
		boolean exit=repo.existsById(id);
		return exit;
	}

	@Override
	public void deleteComapny(Long id) {
		repo.deleteById(id);
	}

}
