package com.ps.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name="company_tab")
@DynamicUpdate
public class Company {
	
	@Id
	@Column(name="com_id_col")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(name="com_name_col")
	private String name;
	
	@Column(name="com_regid_col")
	private String cregId;
	
	@Column(name="com_type_col")
	private String ctype;
	
	@Column(name="com_parent_col")
	private String parentOrg;
	
	@Column(name="com_mode_col")
	private String modeOfOperate;
	
	@Column(name="com_service_code_col")
	private String serviceCode;
	
	
	@OneToOne(
			cascade = CascadeType.ALL,
			fetch = FetchType.EAGER)
	@JoinColumn(name="aidFk")
	private Address addr;//HAS-A


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getCregId() {
		return cregId;
	}


	public void setCregId(String cregId) {
		this.cregId = cregId;
	}


	public String getCtype() {
		return ctype;
	}


	public void setCtype(String ctype) {
		this.ctype = ctype;
	}


	public String getParentOrg() {
		return parentOrg;
	}


	public void setParentOrg(String parentOrg) {
		this.parentOrg = parentOrg;
	}


	public String getModeOfOperate() {
		return modeOfOperate;
	}


	public void setModeOfOperate(String modeOfOperate) {
		this.modeOfOperate = modeOfOperate;
	}


	public String getServiceCode() {
		return serviceCode;
	}


	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}


	public Address getAddr() {
		return addr;
	}


	public void setAddr(Address addr) {
		this.addr = addr;
	}


	@Override
	public String toString() {
		return "Company [id=" + id + ", name=" + name + ", cregId=" + cregId + ", ctype=" + ctype + ", parentOrg="
				+ parentOrg + ", modeOfOperate=" + modeOfOperate + ", serviceCode=" + serviceCode + ", addr=" + addr
				+ "]";
	}

	public Company() {
	}
	
	
	
	
	
		
}