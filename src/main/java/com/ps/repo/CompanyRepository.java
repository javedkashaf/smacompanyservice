package com.ps.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ps.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Long>{

}
