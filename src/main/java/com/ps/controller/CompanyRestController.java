package com.ps.controller;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ps.entity.Company;
import com.ps.exception.CompanyNotFoundException;
import com.ps.serviceImpl.CompanyServiceImpl;



@RestController
@RequestMapping("/company")
public class CompanyRestController {
	
	 private  static final Logger log = LoggerFactory.getLogger(CompanyRestController.class);
	
	@Autowired
	private CompanyServiceImpl service;

	//1. create company
	@PostMapping("/create")
	public ResponseEntity<String> createCompany(
			@RequestBody Company company
			) 
	{
		log.info("ENTERD INTO SAVE METHOD");
		ResponseEntity<String> response = null;
		try {
			Long id = service.createCompany(company);
			response = ResponseEntity.ok("CREATED WITH ID : "+ id);
			log.info("COMPANY IS CREATED {}."+id);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error Created in Employe Saving");
		}
		return response;
	}
	
	
	
	//1. create company
	@GetMapping("/companies")
	public ResponseEntity<List<Company>> getAllCompanies(
			@RequestBody Company company) 
	{
		log.info("ENTERD INTO LIST METHOD");
		ResponseEntity<List<Company>> response = null;
		try {
			List<Company> list = service.getAllCompanies();
			response = ResponseEntity.ok(list);
			log.error("FETCHING ALL DATA SUCCESS");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("ERROR OCCURED IN EMPLOYE LIST");
		}
		log.error("FETCHING ALL THE COMPANY DETAILS");
		return response;
	}
	
	
	//1. create company
	@GetMapping("/company/{id}")
	public ResponseEntity<Company> getComapny(
			@PathVariable Long id) 
	{
		log.info("ENTERD INTO COMPANY METHOD");
		ResponseEntity<Company> response = null;
		try {
			Company cob = service.getOneCompany(id);
			response = ResponseEntity.ok(cob);
			log.error("FETCHING COMPANY SUCCESS");
		} catch (CompanyNotFoundException e) {
		
			log.error(e.getMessage());
			throw e; 
		}
		log.error("FETCHING COMPANY DETAILS SUCCESS");
		return response;
	}
	
	
	
	//1. create company
	@GetMapping("/delete/{id}")
	public ResponseEntity<String> DeleteCompany(
			@PathVariable Long id) 
	{
		log.info("ENTERD INTO COMPANY DELETED METHOD");
		ResponseEntity<String> response = null;
		try {
			boolean exist = service.isExist(id);
			if(exist) {
				service.deleteComapny(id);
			}
			response = ResponseEntity.ok("Company Deleted Successfully");
			log.error("DELETEING COMPANY DETAILS SUCCESS");
		} catch (CompanyNotFoundException e) {
		
			log.error(e.getMessage());
			throw e; 
		}
		log.error("COMPANY DELETED SUCCESSFULLY");
		return response;
	}
	

}
